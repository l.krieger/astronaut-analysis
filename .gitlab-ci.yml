# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: CC0-1.0

stages:
    - lint
    - test
    - run 
    - deploy

default:
    image: python:3.11
    interruptible: true

variables:
    CACHE_PATH: "$CI_PROJECT_DIR/.cache"
    POETRY_VIRTUALENVS_PATH: "$CACHE_PATH/venv"
    POETRY_CACHE_DIR: "$CACHE_PATH/poetry"
    PIP_CACHE_DIR: "$CACHE_PATH/pip"

.dependencies:
    before_script:
        - pip install -U pip
        - pip install poetry
        - poetry install
    cache: 
        key: 
            files:
                - poetry.lock
            prefix: $CI_JOB_IMAGE
        paths:
            - $CACHE_PATH

.base_test:
    extends: .dependencies
    stage: test
    script:
        - poetry run pytest tests/

.default_branch_only:
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH


license_compliance:
    extends: .dependencies
    stage: lint
    script:
        - poetry run reuse lint

codestyle:
    extends:
        - .dependencies
    stage: lint
    script:
        - poetry run black --check --diff .
        - poetry run isort --check --diff .

test:python:
    extends: .base_test
    image: python:${PYTHON_VERSION}
    parallel:
        matrix:
            - PYTHON_VERSION: ["3.9","3.10","3.11"]


run_astronaut_analysis:
    extends: 
        - .dependencies
        - .default_branch_only
    stage: run 
    script:
        - poetry run python -m astronaut_analysis
    
    artifacts:
        paths:
            - results/ 


pages:
    extends: .default_branch_only
    image: alpine:latest
    stage: deploy
    before_script:
        - echo "Skipping before_script section"
    script:
        - mkdir public/
        - cp results/age_histogram.png public/age_histogram.png
    dependencies:
        - run_astronaut_analysis
    artifacts:
        paths:
            - public/
